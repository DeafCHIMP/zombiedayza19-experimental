Fixes by Riles

Everything is XML only, so it pushes to clients from servers! :-)

Full List of Changes and Fixes (since version 1.04)

Block Improvements/Changes/Additions
-Many blocks (roof parts, trims, bridge ramps, half blocks, pillars with tops, etc) are now buildable and upgradeable
-Bars are now a variant block, allows you to switch between centered and regular.
-Bars, Catwalks, Railings can be upgraded to a steel variant
-Railings have been updated (thin wood bar, upgrade path improved to follow models)
-Commercial/House doors can be built (accounting for brass) and upgraded to iron doors
-Broken gravestones can be repaired to upright/fixed version


Gameplay Changes
-Localization file updated to include new blocks and improved for better sorting of item parts and bundles.
-Animal corpses stick around for 20 minutes instead of 5
-Yellow Loot bags from zombies stick around Significantly longer
-Lanterns, Burning Barrels, "good" tires and yellow flashlights can be picked up (flashlight turns into regular one)
-Flashlights stack to 50, like torches
-Vehicle Wheel Changes
	-Make a tire with acid OR pick up "good" tires found in the world
	-Make a wheel with a tire and mechanical parts
-Archery Projectile Recipe changes
	-All require feathers, not plastic
	-Feathers can be made from plastic
	-Bolts and Arrows can be converted back and forth (exploding has a difference)
-First Aid Bandages can also be made directly from a bandage
-Car Salvaging, turned off damage passthrough as that limits salvaging amount
-Anything that can be built as a bundle, can be rebundled
-Clay Soil is now a bundleable resource through Art Of Mining Pallets perk







	
	
	

