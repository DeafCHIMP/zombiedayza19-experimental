License And Terms Of Use

1) You are free to download And play in Single Player, Your are also allowed to Upload those Files to a server in order 
   to play On-line Assuming you own a Legal Copy of the game (7 Days To Die)

2) You are NOT Permitted to Alter any Code, Change or Rename any Models or Redistribute.

3) You are also free to tailor their experience to their needs by altering the XML files. When hosting a server or game
   with such altered contents, you are NOT permitted to Share these modifications without expression permission.
   You MUST seek permission from myself before files, Code, Models or Assets that are contained within my mods are
   shared other than on your PC or on a Dedicated server for private use.

4) Users are NOT allowed to take the code from this mod to add to another mod in any way without first asking Me.
   For explicit permission. This applies too taking (parts of) the code, XML or assets as well as altered versions of these.

5) This version of the mod is special for the modPack (the overhaul) ZombieDayz from 𝕾𝖊𝖈𝖍𝖘𝖙𝖊𝖗𝖛𝖊𝖗𝖘𝖚𝖈𝖍 (Discord sechsterversuch#5780).
   It may only be distributed as part of this modPack/Overhaul. As part of this modPack/Overhaul it may
   can be changed and adapted without any limitations as long as the following listed files remain unchanged:
		- EN_License_Information.md
		- EN_Readme.md
		- GER_License_Information.md
		- GER_Readme.md
		- mod.xml
		- ModInfo.xml
		- Contact_info.md
		
All Rights Reserved, 2020 by FenrisGamingWolf